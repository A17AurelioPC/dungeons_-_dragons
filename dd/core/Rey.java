package dd.core;

import java.util.Arrays;

public class Rey extends Personaje {
    private int nAtaques = 3;
    private int salud = 2000;

    public Rey(String nom) {
        super(nom);
        setSalud(salud);
    }

    @Override
    public int[] ataca(Personaje p) {
        int[] arAta = new int[nAtaques];
        if (getAtaque() != null) {
            for (int i = 0; i < arAta.length; i++) {
                arAta[i] = getAtaque().lanzaAtaque(p);
            }
        }
        return Arrays.copyOf(arAta, arAta.length);
    }

    @Override
    public int updateSalud(int i) {
        int x = (getSalud() - i);
        setSalud(x);
        return x;
    }

    @Override
    public String toString() {
        return "[" + this.getNombre() + ":" + this.getSalud() + "]";
    }

}
