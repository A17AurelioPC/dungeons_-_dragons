package dd.main;

import java.util.Random;

import dd.core.Ataque;
import dd.core.AtaqueArco;
import dd.core.AtaqueEspada;
import dd.core.Ataquecuchillo;
import dd.core.Caballero;
import dd.core.Personaje;
import dd.core.Rey;
import dd.core.Troll;

public class DDApp {
    public static Random n = new Random();
    public static Ataque espada = new AtaqueEspada();
    public static Ataque arco = new AtaqueArco();
    public static Ataque cuchillo = new Ataquecuchillo();
    public static Personaje[] hombres, trolls;

    public static void main(String[] args) {
        hombres = creaHombres();
        trolls = creaTrolls();
        boolean vacioh = false, vaciot = false;
        while (true) {
            ronda(hombres);

            ronda(trolls);

            vacioh = muerto(hombres);
            vaciot = muerto(trolls);

            if (vacioh || vaciot) {
                System.out.println(
                        "Finalmente, el ejército de" + ((vaciot) ? "l Rey Arturo " : " los trolls ") + "venció!!");
                System.out.println("Los supervivientes de la batalla fueron:");
                if (vaciot) {
                    for (Personaje p : hombres) {
                        System.out.print(p);
                    }
                } else {
                    for (Personaje p : trolls) {
                        System.out.print(p);
                    }
                }
                break;
            }
        }

    }

    private static Personaje[] creaHombres() {
        Personaje[] hombres = new Personaje[3];
        hombres[0] = new Rey("Arturo");
        hombres[1] = new Caballero("Lancelot");
        hombres[2] = new Caballero("Percival");

        hombres[0].setAtaque(espada);
        hombres[1].setAtaque(espada);
        hombres[2].setAtaque(arco);
        return hombres;
    }

    private static Personaje[] creaTrolls() {
        Personaje[] trolls = new Personaje[n.nextInt(12 - 2) + 2];
        int tamaño = trolls.length;

        for (int i = 0; i < tamaño; i++) {
            trolls[i] = new Troll("Troll " + (i + 1));
            trolls[i].setAtaque((n.nextInt(2) == 0) ? espada : cuchillo);
        }
        return trolls;
    }

    private static void ronda(Personaje[] p) {

        for (Personaje personaje : p) {

            if (personaje instanceof Rey) {
                if (personaje != null) {
                    int[] res;
                    int ale;
                    while (true) {
                        ale = n.nextInt(trolls.length);
                        if (trolls[ale] != null) {
                            res = personaje.ataca(trolls[ale]);
                            break;
                        }
                    }
                    System.out.println(personaje + " lucha contra " + trolls[ale]);
                    salir: for (int i : res) {
                        System.out.printf("%s%s)%n", personaje.getAtaque(), (i != 0) ? -i : "falla");
                        if (trolls[ale].updateSalud(i) <= 0) {
                            System.out.println(trolls[ale] + " muere" + "\n");
                            trolls[ale] = null;
                            break salir;
                        }
                    }
                }

                if (muerto(trolls))
                    break;

            } else if (personaje instanceof Caballero) {
                if (personaje != null) {
                    int[] res;
                    int ale;
                    while (true) {
                        ale = n.nextInt(trolls.length);
                        if (trolls[ale] != null) {
                            res = personaje.ataca(trolls[ale]);
                            break;
                        }
                    }
                    System.out.println(personaje + " lucha contra " + trolls[ale]);
                    salir: for (int i : res) {
                        System.out.printf("%s%s)%n", personaje.getAtaque(), (i != 0) ? -i : "falla");
                        if (trolls[ale].updateSalud(i) <= 0) {
                            System.out.println(trolls[ale] + " muere" + "\n");
                            trolls[ale] = null;
                            break salir;
                        }
                    }
                }
                if (muerto(trolls))
                    break;

            } else if (personaje instanceof Troll) {
                if (personaje != null) {
                    int[] res;
                    int ale;
                    while (true) {
                        ale = n.nextInt(hombres.length);
                        if (hombres[ale] != null) {
                            res = personaje.ataca(hombres[ale]);
                            break;
                        }
                    }
                    System.out.println(personaje + " lucha contra " + hombres[ale]);
                    salir: for (int i : res) {
                        System.out.printf("%s%s)%n", personaje.getAtaque(), (i != 0) ? -i : "falla");
                        if (hombres[ale].updateSalud(i) <= 0) {
                            System.out.println(hombres[ale] + " muere" + "\n");
                            hombres[ale] = null;
                            break salir;
                        }
                    }
                }
                if (muerto(hombres))
                    break;
            }
        }
    }

    private static Personaje luchaCon(Personaje[] p) {
        int ale;
        while (true) {
            ale = n.nextInt(p.length);
            if (p[ale] != null) {
                break;
            }
        }
        return p[ale];
    }

    private static boolean muerto(Personaje[] p) {
        boolean muerto = true;
        for (Personaje personaje : p) {
            if (personaje != null)
                muerto = false;
        }
        return muerto;
    }
}
