package dd.core;

import java.util.Arrays;
import java.util.Random;

public class Caballero extends Personaje {

    private Random n = new Random();
    private int nAtaques = 2;

    public Caballero(String nom) {
        super(nom);
        setSalud(n.nextInt(1500 - 1000) + 1000);
    }

    @Override
    public int[] ataca(Personaje p) {
        int[] arAta = new int[nAtaques];
        if (getAtaque() != null) {
            for (int i = 0; i < arAta.length; i++) {
                arAta[i] = getAtaque().lanzaAtaque(p);

            }
        }
        return Arrays.copyOf(arAta, arAta.length);
    }

    @Override
    public int updateSalud(int i) {
        int x = (getSalud() - i);
        setSalud(x);
        return x;
    }

    @Override
    public String toString() {
        return "[" + this.getNombre() + ":" + this.getSalud() + "]";
    }
}
