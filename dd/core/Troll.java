package dd.core;

import java.util.Arrays;
import java.util.Random;

public class Troll extends Personaje {
    private Random n = new Random();
    private int nAtaques = 1;
    
    public Troll(String nom) {
        super(nom);
        setSalud(n.nextInt(1000 - 500) + 500);
    }

    public int updateSalud(int x){
        int n = (getSalud()-x);
        setSalud(n);
        return n;
    }


    @Override
    public int[] ataca(Personaje p) {
        int[] arAta = new int[nAtaques];
        if(getAtaque()!=null)
            arAta[0]=getAtaque().lanzaAtaque(p);
        return Arrays.copyOf(arAta, arAta.length);
    }

    @Override
    public String toString() {
        return "[" + this.getNombre() + ":" + this.getSalud() + "]";
    }
}
