package dd.core;

import java.util.Random;

public class Ataquecuchillo implements Ataque {
    private int valor;
    private boolean acierto = true;
    private Random n = new Random();

    @Override
    public int lanzaAtaque(Personaje p) {
        acierto = (n.nextInt(5) > 1) ? true : false;
        if (acierto) {
            valor = (int) (Math.random() * 25);
        } else {
            valor = 0;
        }
        return valor;
    }

    @Override
    public String toString() {
        return "Ataque con cuchillo (";
    }
}
