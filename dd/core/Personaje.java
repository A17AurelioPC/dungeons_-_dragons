package dd.core;

public abstract class Personaje {
    private String nombre;
    private Ataque ataque;
    private int salud;

    public Personaje(String nom) {
        this.nombre = nom;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Ataque getAtaque() {
        return this.ataque;
    }

    public int getSalud() {
        return this.salud;
    }


    public void setAtaque(Ataque a) {
        this.ataque = a;
    }

    public int updateSalud(int n) {
        salud += n;
        return salud;
    }

    public abstract int[] ataca(Personaje p);

    @Override
    public String toString() {
        return "[" + this.getClass().getName() + this.nombre + ":" + this.salud + "]";
    }
}
